/*
 * QDiscord - An unofficial C++ and Qt wrapper for the Discord API.
 * Copyright (C) 2016-2017 george99g
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.	 If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QDISCORDATTACHMENT_HPP
#define QDISCORDATTACHMENT_HPP

#include "qdiscord.d/external/optional/optional.hpp"
#include "qdiscord.d/models/qdiscordid.hpp"
#include <QJsonObject>

class QDiscordAttachment
    : public QDiscordModelBase<QDiscordAttachment>
    , public QDiscordModel::CompareById<QDiscordAttachment>
{
    Q_GADGET

    Q_PROPERTY(QDiscordID dId READ id WRITE setId)
    Q_PROPERTY(QString filename READ filename WRITE setFilename)
    Q_PROPERTY(qint64 size READ size WRITE setSize)
    Q_PROPERTY(QString url READ url WRITE setUrl)
    Q_PROPERTY(QString proxyUrl READ proxyUrl WRITE setProxyUrl)
    Q_PROPERTY(
        std::experimental::optional<int> height READ height WRITE setHeight)
    Q_PROPERTY(std::experimental::optional<int> width READ width WRITE setWidth)

public:
    static QSharedPointer<QDiscordAttachment>
    fromJson(const QJsonObject& object);

    QDiscordAttachment(const QJsonObject& object);
    QDiscordAttachment() = default;

    void deserialize(const QJsonObject& object);
    QJsonObject serialize() const;

    QDiscordID id() const { return _id; }
    void setId(QDiscordID id) { _id = id; }
    QString filename() const { return _filename; }
    void setFilename(const QString& filename) { _filename = filename; }
    qint64 size() const { return _size; }
    void setSize(qint64 size) { _size = size; }
    QString url() const { return _url; }
    void setUrl(const QString& url) { _url = url; }
    QString proxyUrl() const { return _proxyUrl; }
    void setProxyUrl(const QString& proxyUrl) { _proxyUrl = proxyUrl; }
    std::experimental::optional<int> height() const { return _height; }
    void setHeight(const std::experimental::optional<int>& height)
    {
        _height = height;
    }
    void resetHeight() { _height.reset(); }
    std::experimental::optional<int> width() const { return _width; }
    void resetWidth() { _width.reset(); }
    void setWidth(const std::experimental::optional<int>& width)
    {
        _width = width;
    }

    template<class Action>
    void map(Action& a)
    {
        using namespace QDiscordModel;
        field(a, _id, "id");
        field(a, _filename, "filename");
        field(a, _size, "size");
        field(a, _url, "url");
        field(a, _proxyUrl, "proxy_url");
        field(a, _height, "height");
        field(a, _width, "width");
    }

    template<class Action>
    void map(Action& a) const
    {
        using namespace QDiscordModel;
        field(a, _id, "id");
        field(a, _filename, "filename");
        field(a, _size, "size");
        field(a, _url, "url");
        field(a, _proxyUrl, "proxy_url");
        field(a, _height, "height");
        field(a, _width, "width");
    }

private:
    QDiscordID _id;
    QString _filename;
    qint64 _size = -1;
    QString _url;
    QString _proxyUrl;
    std::experimental::optional<int> _height;
    std::experimental::optional<int> _width;
};

Q_DECLARE_METATYPE(QDiscordAttachment)

#endif // QDISCORDATTACHMENT_HPP
